/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package algortimossorts;

/**
 *
 * @author Yuri
 */
public class AlgortimosSorts {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int tamanhoarray = 1000, numeroarrays =  5000;
        int [][] array = new int [numeroarrays] [tamanhoarray];
        long [][] arraydata = new long [numeroarrays + 1] [2];
        randomizarMatriz(array, tamanhoarray, numeroarrays);
        testarArray(array, "bubble", numeroarrays, arraydata);
        randomizarMatriz(array, tamanhoarray, numeroarrays);
        testarArray(array, "selection", numeroarrays, arraydata);
        randomizarMatriz(array, tamanhoarray, numeroarrays);
        testarArray(array, "insertion", numeroarrays, arraydata);
        randomizarMatriz(array, tamanhoarray, numeroarrays);
        testarArray(array, "shell", numeroarrays, arraydata);
        randomizarMatriz(array, tamanhoarray, numeroarrays);
        testarArray(array, "quick", numeroarrays, arraydata);
        
    }
    
    public static void testarArray(int [][] matriz, String tipoarray, int numeroarrays, long [][] matrizdata){
        Sorting sorts = new Sorting();
        long startTime, endTime, duration = 0, comparations = 0;
        switch(tipoarray){
            case "bubble":
                System.out.println("Bubble!");
                for(int j = 0; j < numeroarrays; j++){
                    startTime = System.currentTimeMillis();
                    matrizdata[j][1] = sorts.bubbleSort(matriz[j]);
                    endTime = System.currentTimeMillis();
                    matrizdata[j][0] = endTime - startTime;
                    duration += matrizdata[j][0];
                    comparations += matrizdata[j][1];
                }
                System.out.println("Ms: " + duration + " Média: " + (double)duration/numeroarrays);
                System.out.println("Comparações: " +comparations + " Média: " + (double)comparations/numeroarrays);
                //imprimirArray(matriz[4999]);
                break;
            case "selection":
                System.out.println("Selection!");
                for(int j = 0; j < numeroarrays; j++){
                    startTime = System.currentTimeMillis();
                    matrizdata[j][1] = sorts.selectionSort(matriz[j]);
                    endTime = System.currentTimeMillis();
                    matrizdata[j][0] = endTime - startTime;
                    duration += matrizdata[j][0];
                    comparations += matrizdata[j][1];
                }
                System.out.println("Ms: " + duration + " Média: " + (double)duration/numeroarrays);
                System.out.println("Comparações: " + comparations + " Média: " + (double)comparations/numeroarrays);
                //imprimirArray(matriz[4999]);
                break;
            case "insertion":
                System.out.println("Insertion!");
                for(int j = 0; j < numeroarrays; j++){
                    startTime = System.currentTimeMillis();
                    matrizdata[j][1] = sorts.insertionSort(matriz[j]);
                    endTime = System.currentTimeMillis();
                    matrizdata[j][0] = endTime - startTime;
                    duration += matrizdata[j][0];
                    comparations += matrizdata[j][1];
                }
                System.out.println("Ms: " + duration + " Média: " + (double)duration/numeroarrays);
                System.out.println("Comparações: " + comparations + " Média: " + (double)comparations/numeroarrays);
                //imprimirArray(matriz[4999]);
                break;
            case "shell":
                System.out.println("Shell!");
                for(int j = 0; j < numeroarrays; j++){
                    startTime = System.currentTimeMillis();
                    matrizdata[j][1] = sorts.shellSort(matriz[j]);
                    endTime = System.currentTimeMillis();
                    matrizdata[j][0] = endTime - startTime;
                    duration += matrizdata[j][0];
                    comparations += matrizdata[j][1];
                }
                System.out.println("Ms: " + duration + " Média: " + (double)duration/numeroarrays);
                System.out.println("Comparações: " + comparations + " Média: " + (double)comparations/numeroarrays);
                //imprimirArray(matriz[4999]);
                break;
            case "quick":
                System.out.println("Quick!");
               for(int j = 0; j < numeroarrays; j++){
                    startTime = System.currentTimeMillis();
                    sorts.quick_sort(matriz[j], 0, matriz[j].length - 1 );
                    endTime = System.currentTimeMillis();
                    matrizdata[j][0] = endTime - startTime;
                    duration += matrizdata[j][0];
                }
                System.out.println("Ms: " + duration + " Média: " + (double)duration/numeroarrays);
                System.out.println("Comparações: " + sorts.getCounterQuick() + " Média: " + (double)sorts.getCounterQuick()/numeroarrays);
                //imprimirArray(matriz[4999]);
                break;
        }
    }
    
    public static void randomizarMatriz(int [][] array, int tamanhoarray, int numeroarrays){
        int max = 100 * 100;
        for(int j = 0; j < numeroarrays; j++){
                for(int i = 0; i < tamanhoarray; i++){
                    array[j][i] = 0 + (int)(Math.random() * ((max - 0) + 1));
                    //System.out.println("Pos:"+ (i+1) + " Num:"+ array[j][i]);
                }
            }
    }
    
     public static void imprimirArray(int [] array){
        for(int i = 0; i < array.length; i++){
            System.out.println(array[i]);
        }
    }
}

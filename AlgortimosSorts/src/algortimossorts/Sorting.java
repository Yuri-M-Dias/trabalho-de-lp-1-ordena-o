/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package algortimossorts;

/**
 *
 * @author Yuri
 */
public class Sorting {
    int counter = 0;
    public int bubbleSort(int array[]){
        int contador = 0;
        int J = 0, aux;
        boolean troca = true;
        while(troca){
            troca = false;
            J += 1;
            for(int i = 0; i < array.length - J; i++){
                if(array[i] > array[i+1]){
                    aux = array[i+1];
                    array[i+1] = array[i];
                    array[i] = aux;
                    troca = true;
                }
                contador++;
            }
        }
        return contador;
    }
    
    public int insertionSort(int array[]){
       int contador = 0;
       for(int i = 2; i < array.length; i++){
           int chave = array[i];
           int J = i -1;
           while((J > 0) && (array[J] > chave)){
               array[J+1] = array[J];
               J -= 1;
               contador++;
           }
           array[J+1] = chave;
        }
        return contador;
    }
    
    public int selectionSort(int array[]){
       int contador = 0;
       for(int i = 0; i < array.length; i++){
           int menor = i;
           for(int j = i; j < array.length; j++){
               if(array[j] < array[menor]) menor = j;
               contador++;
           }
           if(menor != i){
              int aux = array[menor];
              array[menor] = array[i];
              array[i] = aux;
           }
        }
        return contador;
    }
    
    public int shellSort(int array[]){
        int contador = 0;
        int h = 1;
        int n = array.length;
        while(h < n)
                h = h * 3 + 1;
        h = h / 3;
        int c, j;
        while (h > 0) {
            for (int i = h; i < n; i++) {
                c = array[i];
                j = i;
                while (j >= h && array[j - h] > c) {
                    array[j] = array[j - h];
                    j = j - h;
                    contador++;
                }
                array[j] = c;
            }
            h = h / 2;
        }
       return contador;
    }
    
    public void quick_sort(int []array,int esq, int dir) {
        int meio;

        if (esq < dir) {
          meio = partition(array, esq, dir);
          quick_sort(array, esq, meio - 1);
          quick_sort(array, meio + 1, dir);
        }
    }
 
    public int partition(int []v, int ini, int fim) {
        int pivo, topo, i;
        pivo = v[ini];
        topo = ini;

        for (i = ini + 1; i <= fim; i++) {
          if (v[i] < pivo) {
            v[topo] = v[i];
            v[i] = v[topo + 1];
            topo++;
          }
          counter++;
        }
        v[topo] = pivo;
        return topo;
      }
    
    public int getCounterQuick(){
        return counter;
     }
    }
